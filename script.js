/*
Теоретичні питання
1.Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
setTimeout() спрацьовує один раз через певний проміжок часу, а от setInterval() дає можливість повторювати функцію не один раз через заданий проміжок часу.
2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Коли у функцію setTimeout() ми передамо нульову затримку, то вона спрацює відразу, але після завершення поточного скрипту.
3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Щоб не займати зайву пам'ять браузера.
*/

const images = [
    "images/image1.jpg",
    "images/image2.jpg",
    "images/image3.JPG",
    "images/image4.png"
];
let currentIndex = 0;
let intervalId;

const imageElements = document.querySelectorAll(".image-to-show");
const stopButton = document.getElementById("stopButton");
const resumeButton = document.getElementById("resumeButton");

function showImage() {
    imageElements.forEach(image => image.style.display = "none");
    imageElements[currentIndex].style.display = "block";
    currentIndex = (currentIndex + 1) % images.length;
}

function startSlideshow() {
    showImage();
    intervalId = setInterval(showImage, 3000);
}

startSlideshow();

stopButton.addEventListener("click", () => {
    clearInterval(intervalId);
});

resumeButton.addEventListener("click", () => {
    startSlideshow();
});
